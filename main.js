// pc.script.attribute('outspeed', 'number', 10, {min: 0, max: 100})

// pc.script.create("main", function (app) {

//     var Main = function (entity) {
//         this.entity = entity;
//         this.suck = 10
//         this.outspeed = entity.outspeed
//     };

//     var outspeed = this.outspeed

//     Main.prototype = {
//         initialize: function () {
//             // console.log('Main initialize this.entity', this.entity)
//             console.log('Main initialize this.suck', this.suck)
//             console.log('Main initialize this.outspeed', this.outspeed)
//             console.log('Main initialize outspeed', outspeed)
//             console.log('Main initialize this', this)
//             // console.log('Main initialize Main', Main)
//             console.log('Main initialize app', app)
//         },

//         update: function (dt) {
//             // console.log('Main update dt? ', dt)
//         },

//         onAttributeChanged: function (name, oldValue, newValue) {
//             console.log(`changed: ${name} old: ${oldValue} new: ${newValue}`)
//         }
//     };

//     return Main;
// });

// attrs
pc.script.attribute('outspeed', 'number', 10, {min: 0, max: 100})

const Main = function (entity) {
    this.entity = entity;
    this.suck = 10
    this.outspeed = entity.outspeed
};

Main.prototype = {
    initialize: function () {
        console.log('Main initialize pc', pc)
        console.log('Main initialize this.suck', this.suck)
        console.log('Main initialize this.outspeed', this.outspeed)
        console.log('Main initialize this', this)
    },

    update: function (dt) {
        // console.log('Main update dt? ', dt)
    },

    onAttributeChanged: function (name, oldValue, newValue) {
        console.log(`changed: ${name} old: ${oldValue} new: ${newValue}`)
    }
};

// app
pc.script.create('main', () => Main)
